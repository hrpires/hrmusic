﻿
function uploadServer(arquivo) {
    var form = new FormData();
    form.append('file', arquivo);
    if (IP_SERVIDOR === null) {
        getConfig();
    }
    let response = $.ajax({
        url: IP_SERVIDOR + '/WebService/rest/upload',
        type: 'POST',
        data: form,
        cache: false,
        contentType: false,
        processData: false
    });
    console.log(response);
    response.then(function (sucesso) {
        closeLoading();
        alert("Upload realizado com sucesso");
        document.getElementById("tempo").innerHTML = "00:00:00";
        setStorageResultados(sucesso);
        window.location.href = 'resultados.html';
    }, function (error) {
        closeLoading();
        alert("Erro ao realizar upload no servidor");
        document.getElementById("tempo").innerHTML = "00:00:00";
    });
}

function getArquivoFileSystem(nomeArq) {
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dir) {
        var fileName = nomeArq + ".wav";
        var filePath;
        var arquivo;
        dir.getFile(fileName, { create: false }, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function () {
                    console.log("Arquivo carregado");
                };
                reader.readAsText(file);
                filePath = file.localURL;
            }, function (erro) {
                alert("Erro : " + erro);
            });

            var xhr = new XMLHttpRequest();
            xhr.open('GET', filePath, true);
            xhr.responseType = 'blob';

            xhr.onload = function () {
                if (this.status === 200) {
                    arquivo = new Blob([this.response], { type: 'audio/wav' });
                }
            };
            xhr.send();
            return arquivo;
        });
    }, function (erro) {
        alert("Falha ao tentar acessar o file System " + erro);
    });
}

function uploadServer_Gravar(nomeArq) {
    var file = getArquivoFileSystem(nomeArq);
    uploadServer(file);
}

function uploadServerInput() {
    var file = $("input[name='arq']").get(0).files[0];
    uploadServer(file);
}


