var IP_SERVIDOR;
var inputIP1;
var inputPorta;

var isMobile = {
   
    Android: function () {
        return /Android/i.test(navigator.userAgent);
    },

    BlackBerry: function () {
        return /BlackBerry/i.test(navigator.userAgent);
    },
    iOS: function () {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS());
    }
};

var consoleMessage = function (msg) {
    console.log(msg);
};

Date.prototype.YYYYMMDDHHMMSS = function () {
    function pad2(n) {
        return (n < 10 ? '0' : '') + n;
    }

    return this.getFullYear() +
        pad2(this.getMonth() + 1) +
        pad2(this.getDate()) +
        pad2(this.getHours()) +
        pad2(this.getMinutes()) +
        pad2(this.getSeconds());
};

var getMicrophonePermission = function (onSuccess, onDenied, onError) {
    window.audioinput.checkMicrophonePermission(function (hasPermission) {
        try {
            if (hasPermission) {
                if (onSuccess) onSuccess();
            }
            else {
                window.audioinput.getMicrophonePermission(function (hasPermission, message) {
                    try {
                        if (hasPermission) {
                            if (onSuccess) onSuccess();
                        }
                        else {
                            if (onDenied) onDenied("User denied permission to record: " + message);
                        }
                    }
                    catch (ex) {
                        if (onError) onError("Start after getting permission exception: " + ex);
                    }
                });
            }
        }
        catch (ex) {
            if (onError) onError("getMicrophonePermission exception: " + ex);
        }
    });
};

var createStatusTimer = function () {
    return setInterval(function () {
        if (window.audioinput.isCapturing()) {
            var str = "" +
                new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");

            if (window.totalReceivedData) {
                str += "|Received:" + totalReceivedData;
            }

            if (window.totalPlayedData) {
                str += "|Played:" + totalPlayedData;
            }
            console.log(str);
            setDuracao();
            
        }
    }, 1000);
};

var handleBackBtn = function () {
    if (window.audioinput && window.audioinput.isCapturing()) window.audioinput.stop();
};

function getDuracao(tempoAtual) {
    var aux = tempoAtual.split(":");
    var hora = Number.parseInt(aux[0]);
    var min = Number.parseInt(aux[1]);
    var seg = Number.parseInt(aux[2]);
    var tempoSeg = aux[0];
    var tempoMin = aux[1];
    var tempoHor = aux[2];
    if (seg < 59) {
        seg++;
    } else {
        if (min < 59) {
            min++;
            seg = 0;
        } else {
            hora++;
            min = 0;
            seg = 0;
        }
    }
    if (seg < 10) {
        tempoSeg = "0" + seg;
    } else {
        tempoSeg = seg;
    }
    if (min < 10) {
        tempoMin = "0" + min + ":";
    } else {
        tempoMin = min + ":";
    }
    if (hora < 10) {
        tempoHor = "0" + hora + ":";
    } else {
        tempoHor = hora + ":";
    }
    return tempoHor + tempoMin + tempoSeg;
}

function setDuracao() {
    var tempoAtual = document.getElementById("tempo").innerText;
    var duracao = getDuracao(tempoAtual);
    document.getElementById("tempo").innerHTML = duracao;
}

function controlaButton(isStatus) {
    document.getElementById("start").disabled = !isStatus;
    document.getElementById("stop").disabled = isStatus;
}

function setConfig(json) {
    localStorage.setItem("Config",json);
}

function getConfig() {
    var arquivo = JSON.parse(localStorage.getItem("Config"));
    if (getStorage("Conectado") === null) {
        if (arquivo !== null && arquivo.IP !== null && arquivo.Porta !== null && document.getElementById('ipv4') !== null && document.getElementById('ipv4').value !== null) {
            document.getElementById('ipv4').value = arquivo.IP;
            document.getElementById('porta').value = arquivo.Porta;
            salvarIP();
        } else if (arquivo !== null && arquivo.IP !== null && arquivo.IP !== "" && arquivo.Porta !== null && arquivo.Porta !== "") {
            IP_SERVIDOR = "http://" + arquivo.IP + ":" + arquivo.Porta;
            isIPValido();
        } else {
            notificacao("Status do servidor: Desconectado", "error");
        }
    } else {
        if (arquivo !== null && arquivo.IP !== null && arquivo.Porta !== null && document.getElementById('ipv4') !== null && document.getElementById('ipv4').value !== null) {
            document.getElementById('ipv4').value = arquivo.IP;
            document.getElementById('porta').value = arquivo.Porta;
        } else if (arquivo !== null && arquivo.IP !== null && arquivo.IP !== "" && arquivo.Porta !== null && arquivo.Porta !== "") {
            IP_SERVIDOR = "http://" + arquivo.IP + ":" + arquivo.Porta;
        }
        if (getStorage("Conectado").Conectado) {
            notificacao("Status do servidor: Conectado", "success");
        } else {
            notificacao("Status do servidor: Desconectado", "error");
        }
    }
}

function carregarInputsIP() {
    var arquivo = JSON.parse(localStorage.getItem("Config"));
    var input = document.getElementById('ipv4').value;
    if (arquivo !== null && arquivo.IP !== null && arquivo.Porta !== null && document.getElementById('ipv4') !== null && input === "") {
        document.getElementById('ipv4').value = arquivo.IP;
        document.getElementById('porta').value = arquivo.Porta;
    }

}

function alteracaoInput() {
    sessionStorage.removeItem("Conectado");
    document.getElementById("btnTestSalv").innerText = "Salvar"; 
}

function salvarIP() {
    sessionStorage.removeItem("Conectado");
    inputIP1 = document.querySelector('#ipv4');
    inputPorta = document.querySelector('#porta');
    IP_SERVIDOR = "http://" + inputIP1.value + ":" + inputPorta.value;
    setConfig(JSON.stringify({
        IP: inputIP1.value,
        Porta: inputPorta.value
    }));

    isIPValido();
}

function notificacao(message, type) {
    new Noty({
        text: message,
        type: type,
        timeout: 5000,
        layout: "bottom"
    }).show();
}

function setStorageConectado(value) {
    sessionStorage.setItem("Conectado", JSON.stringify({
        Conectado: value
    }));
}

function setStorageResultados(value) {
    sessionStorage.setItem("Resultados", JSON.stringify({
        Resultados: value
    }));
}

function getStorage(nome) {
    return JSON.parse(sessionStorage.getItem(nome));
}

function isIPValido() {
    let status;
    let isAlert = false;
    if (JSON.parse(sessionStorage.getItem("Conectado")) === null) {
        if (inputIP1 !== "" && inputPorta !== "") {
            loading('Verificando conex�o com servidor...');
            isAlert = true;
            status = true;
        }
    } else {
        status = JSON.parse(sessionStorage.getItem("Conectado")).Conectado;
    }
    if (inputIP1 !== "" && inputPorta !== "") {
        if (status) {
            status = false;
            var json = {
                method: 'GET',
                headers: {
                    "Content-type": "text/plain; charset=UTF-8"
                }
            };
            let url = IP_SERVIDOR + "/WebService/rest/upload";
            fetch(url, json).then(function (res) {
                if (res.ok) {
                    if (isAlert) {
                        closeLoading();
                    }
                    notificacao("Status do servidor: Conectado ", "success");

                    if (document.getElementById('ipv4') !== null && document.getElementById('ipv4').value !== null && document.getElementById('porta') !== null && document.getElementById('porta').value !== null) {
                        var json = JSON.stringify({
                            IP: inputIP1.value,
                            Porta: inputPorta.value
                        });
                        setConfig(json);
                    }
                    status = true;
                    setStorageConectado(status);
                } else {
                    if (isAlert) {
                        closeLoading();
                    }
                    notificacao("Status do servidor: Falha na conex�o, verifique as configura��es", "error");

                    status = false;
                    setStorageConectado(status);
                }
            }).catch(function (e) {
                if (isAlert) {
                    closeLoading();
                }
                console.log(e);
                notificacao("Status do servidor: Falha na conex�o, verifique as configura��es", "error");
                status = false;
                setStorageConectado(status);
            });
        } else {
            notificacao("Status do servidor: Desconectado", "error");
            setStorageConectado(status);
        }

    }

}

function temporizador() {
    let valor = 0;
    var intervalo = setInterval(function () {
        valor = document.getElementById('progressProfile').innerText;
        valor = parseInt(valor.replace('s', ''));
        document.getElementById('progressProfile').progressCircle.update(valor + 10);
    }, 1000);

    setTimeout(function () {
        clearInterval(intervalo);
    }, 10000);
    
}

function showMyCustomizedAlert() {
    alert({
        title: 'Aten��o',
        message: 'Deseja inserir um novo endere�o de IP antes da verifica��o ?',
        buttons: [
            {
                label: 'SIM',
                onclick: function () {
                    inputIP1 = "";
                    inputPorta = "";
                    document.getElementById("ipv4").innerHTML = "";
                    document.getElementById("porta").innerHTML = "";
                    closeAlert();
                }
            },
            {
                label: 'N�O',
                onclick: function () {
                    closeAlert();
                }
            }
        ]
    });
}


$('.mascaraIp').mask('099.099.099.099');
