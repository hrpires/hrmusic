var resultEsperado;
var nomeNota;
var resultObtido;
var nomeMusical;
var percentAcerto;
var posicao;
var bar = null;

function ini(pos) {
    posicao = pos;
    var store = getStorage("Resultados");
    var subTitulo = "--� segundo";
    if (store !== null) {
        subTitulo = (pos * 0.2).toFixed(2) + "� segundo(s)";
        var result = store.Resultados[pos-1];
        resultEsperado = result.arResultEsperado;
        resultObtido = result.arResultado;
        nomeNota = result.nomeNota;
        nomeMusical = result.nomeMusical;
        percentAcerto = result.probAcerto;
        
    } else {
        resultEsperado = [];
        resultObtido = [];
        nomeNota = "--";
        nomeMusical = "--";
        percentAcerto = 0;
    }
    document.getElementById('titulo').innerHTML = subTitulo;
    notaReconhecida();
    graficoPorcentagem();
    graficoLinha();
}
function graficoPorcentagem() {
    if (bar === null) {
        bar = new ProgressBar.SemiCircle(porcentagemAcerto, {
            strokeWidth: 6,
            color: '#FFFFFF',
            trailColor: '#eee',
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            svgStyle: null,
            text: {
                value: '',
                alignToBottom: false
            },
            from: { color: '#FFFFFF' },
            to: { color: '#FFFFFF' },
            // Set default step function for all animate calls
            step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
                var value = (percentAcerto).toFixed(2);
                if (value === 0) {
                    bar.setText('0%');
                } else {
                    bar.setText(value + "%");
                }

                bar.text.style.color = state.color;
            }
        });
        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar.text.style.fontSize = '2rem';
    }

    bar.animate(percentAcerto/100);  // Valores de 0.0 ate 1.0
}

function notaReconhecida() {
    document.getElementById("notaResult").innerHTML = nomeMusical;
    document.getElementById("notaNome").innerHTML = nomeNota;
}

function graficoLinha() {
    new Chartist.Line('#chartist-line', {
        labels: [0.01, 0.05, 0.1, 0.15, 0.18 , 0.2],
        series: [
            resultEsperado,
            resultObtido
        ]
    });
}

function proximo() {
    var result = getStorage("Resultados");
    if (result !== null && result !== undefined && posicao < result.Resultados.length)
        ini(++posicao);
}

function voltar() {
    if (posicao > 0)
        ini(--posicao);
}

