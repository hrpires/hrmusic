﻿
// Capture configuration object
var captureCfg = {};

// Audio Buffer
var audioDataBuffer = [];

// Timer
var timerInterVal;

var objectURL = null;

// Info/Debug
var totalReceivedData = 0;

window.URL = window.URL || window.webkitURL;


function onAudioInputCapture(evt) {
    try {
        if (evt && evt.data) {
            // Increase the debug counter for received data
            totalReceivedData += evt.data.length;
            // Add the chunk to the buffer
            audioDataBuffer = audioDataBuffer.concat(evt.data);
        }
        else {
            alert("Unknown audioinput event: " + JSON.stringify(evt));
        }
    }
    catch (ex) {
        alert("onAudioInputCapture ex: " + ex);
    }
}

function onAudioInputError(error) {
    alert("onAudioInputError event recieved: " + JSON.stringify(error));
}

var startCapture = function () {
    //getConfig();
    if (JSON.parse(sessionStorage.getItem("Conectado")).Conectado) {
        var ex = false;
        var noty = new Noty({
            text: "Posicione-se em 10 segundos iniciará a gravação",
            type: "information",
            timeout: 10000,
            layout: "bottom",
            callbacks: {
                beforeShow: function () { },
                onShow: function () { },
                afterShow: function () { },
                onClose: function () { },
                afterClose: function () {
                    try {
                        if (window.audioinput && !window.audioinput.isCapturing()) {
                            controlaButton(false);
                            captureCfg = {
                                sampleRate: 44100,
                                bufferSize: 16384,
                                channels: 1,
                                format: audioinput.FORMAT.PCM_16BIT,
                                audioSourceType: audioinput.AUDIOSOURCE_TYPE.DEFAULT
                            };

                            // See utils.js
                            getMicrophonePermission(function () {
                                consoleMessage("Microphone input starting...");
                                window.audioinput.start(captureCfg);
                                consoleMessage("Microphone input started!");

                                if (objectURL) {
                                    URL.revokeObjectURL(objectURL);
                                }

                                timerInterVal = createStatusTimer(); // See utils.js
                            }, function (deniedMsg) {
                                consoleMessage(deniedMsg);
                            }, function (errorMsg) {
                                consoleMessage(errorMsg);
                            });
                        }
                    }
                    catch (e) {
                        alert("Erro na gravaçãos: " + e);
                    }
                },
                onHover: function () { },
                onTemplate: function () { }
            }
        }).show();
    } else {
        notificacao("Status do servidor: Desconectado", "error");
    }


};

var stopCapture = function () {
    try {
        if (window.audioinput && window.audioinput.isCapturing()) {
          
            if (timerInterVal) {
                clearInterval(timerInterVal);
            }

            if (window.audioinput) {
                window.audioinput.stop();
            }

            totalReceivedData = 0;
         
            consoleMessage("Encoding WAV...");
            var encoder = new WavAudioEncoder(window.audioinput.getCfg().sampleRate, window.audioinput.getCfg().channels);
            encoder.encode([audioDataBuffer]);

            blob = encoder.finish("audio/wav");

            var reader = new FileReader();

            reader.onload = function (evt) {
                var audio = document.createElement("AUDIO");
                audio.controls = true;
                audio.src = evt.target.result;
                audio.type = "audio/wav";
                consoleMessage("Audio created");
                audioDataBuffer = [];
            };

            reader.readAsDataURL(blob);

            window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dir) {
                var fileName = new Date().YYYYMMDDHHMMSS() + ".wav";
                dir.getFile(fileName, { create: true }, function (file) {
                    file.createWriter(function (fileWriter) {
                        fileWriter.write(blob);

                        // Add an URL for the file
                        var a = document.createElement('a');
                        var linkText = document.createTextNode(file.toURL());
                        a.appendChild(linkText);
                        a.title = file.toURL();
                        a.href = file.toURL();
                        a.target = '_blank';
                        consoleMessage("File created!" + a.href);
                        loading('Realizando upload para servidor...');
                        uploadServer(blob);
                        controlaButton(true);
                    }, function () {
                        alert("Erro ao criar arquivo" );
                    });
                });
            });
        }
    }
    catch (e) {
        alert("Erro ao tentar parar gravação: " + e);
    }
};

var onDeviceReady = function () {
    if (window.cordova && window.audioinput) {
      
        window.addEventListener('audioinput', onAudioInputCapture, false);
        window.addEventListener('audioinputerror', onAudioInputError, false);

        //fetch("http://" + IP + ":" + porta + "/WebService/rest/upload").then(function (Response) {
        //    if (Response.ok) {
        //        console.log(Response.statusText);
        //    }
        //});

        consoleMessage("Use 'Start Capture' to begin...");
    }
    else {
        consoleMessage("cordova-plugin-audioinput not found!");
    }
};

if (!window.cordova) {
    console.log("Running on desktop!");
    onDeviceReady();
}
else {
    console.log("Running on device!");
    document.addEventListener('deviceready', onDeviceReady, false);
}